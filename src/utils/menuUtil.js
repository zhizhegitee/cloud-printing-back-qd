import store from '@/store'
export function getMenuListByPath(path) {
    const menuList = store.state.menu.menuList;
    for(let item of menuList){
        if(item.path == path){
            return formatData(item.children)
        }
        if(item.children.length !=0 && item.children[0].isMenu == 1){
            for(let citem of item.children){
                if(citem.path == path){
                    // return citem.children.map(mapitem=>{return mapitem.permissionCode});
                    return formatData(citem.children)
                }
            }
        }
    }
}
function formatData(data){
    let formatData = {
        delete:0,add:0,update:0
    }
    for (const item of data) {
        for (const key in formatData) {
            if(item.permissionCode.indexOf(key) == 0){
                formatData[key] = 1
            }
        }
    }
    return formatData
}