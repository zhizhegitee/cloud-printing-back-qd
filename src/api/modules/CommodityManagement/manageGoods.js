import request from '@/utils/axiosRequest.js'

//获取商品进行分页
export function getPagingCommodity(data) {
    return request({
        url: '/commodity/getPagingCommodity',
        method: 'get',
        params:data
    })
}

//增加商品信息
export function addCommodity(data) {
    return request({
        url: '/commodity/addCommodity',
        method: 'post',
        data:data
    })
}


//根据id获取商品
export function getCommodityById(data) {
    return request({
        url: '/commodity/getCommodityById',
        method: 'get',
        params:data
    })
}
//修改商品信息
export function updateCommodity(data) {
    return request({
        url: '/commodity/updateCommodity',
        method: 'post',
        data:data
    })
}


//删除商品
export function deleteCommodity(data) {
    return request({
        url: '/commodity/deleteCommodity',
        method: 'post',
        data:data
    })
}

//删除多个商品
export function deleteManyCommodity(data) {
    return request({
        url: '/commodity/deleteManyCommodity',
        method: 'post',
        data:data
    })
}
