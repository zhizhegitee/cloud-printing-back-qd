import request from '@/utils/axiosRequest.js'

//获取分页规格
export function getPagingSpecifications(data) {
    return request({
        url: '/specifications/getPagingSpecifications',
        method: 'get',
        params:data
    })
}
//获取所有规格
export function getAllSpecifications() {
    return request({
        url: '/specifications/getAllSpecifications',
        method: 'get',
    })
}
//增加规格
export function addSpecifications(data){
    return request({
        url: '/specifications/addSpecifications',
        method: 'post',
        data:data
    })
}

//删除一条规格
export function deleteSpecifications(data){
    return request({
        url: '/specifications/deleteSpecifications',
        method: 'post',
        data:data
    })
}
//删除多条规格
export function deleteManySpecifications(data){
    return request({
        url: '/specifications/deleteManySpecifications',
        method: 'post',
        data:data
    })
}
//修改规格
export function updateSpecifications(data){
    return request({
        url: '/specifications/updateSpecifications',
        method: 'post',
        data:data
    })
}
