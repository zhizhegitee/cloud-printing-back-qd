import request from '@/utils/axiosRequest.js'
//根据权限获取分页订单评价
export function getPagingStoreEvaluate(data) {
    return request({
      url: 'evaluate/getPagingStoreEvaluate',
      method: 'get',
      params:data
    })
}