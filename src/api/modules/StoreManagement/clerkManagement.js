import request from '@/utils/axiosRequest.js'

//获取所有员工
export function getAllClerk(data) {
  return request({
    url: '/clerk/getAllClerk',
    method: 'get',
    params:data
  })
}

//增加员工
export function addClerk(data) {
  return request({
    url: '/clerk/addClerk',
    method: 'post',
    data:data
  })
}
//删除员工
export function deleteClerk(data) {
  return request({
    url: '/clerk/deleteClerk',
    method: 'post',
    data:data
  })
}
//删除多个员工
export function deleteManyClerk(data) {
  return request({
    url: '/clerk/deleteManyClerk',
    method: 'post',
    data:data
  })
}
