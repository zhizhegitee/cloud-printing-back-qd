import request from '@/utils/axiosRequest.js'

//通过用户名搜索用户
export function getUserByLikeUsername(data) {
  return request({
    url: '/user/getUserByLikeUsername',
    method: 'get',
    params:data
  })
}
//获取一个用户信息
export function getUserById(data) {
  return request({
    url: '/user/getUserById',
    method: 'get',
    params:data
  })
}

//增加用户
export function addUser(data) {
  return request({
    url: '/user/addUser',
    method: 'post',
    data:data
  })
}
//修改用户信息
export function updateUser(data) {
  return request({
    url: '/user/updateUser',
    method: 'post',
    data:data
  })
}
//修改用户密码
export function updateUserPassword(data) {
  return request({
    url: '/user/updateUserPassword',
    method: 'post',
    data:data
  })
}
//删除用户
export function deleteUserById(data) {
  return request({
    url: '/user/deleteUserById',
    method: 'post',
    data:data
  })
}
//删除多个用户
export function deleteUserByIdList(data) {
  return request({
    url: '/user/deleteUserByIdList',
    method: 'post',
    data:data
  })
}
