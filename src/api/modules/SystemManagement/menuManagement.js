import request from '@/utils/axiosRequest.js'

//获取所有菜单
export function getAllMenu() {
  return request({
    url: '/permission/getAllMenu',
    method: 'get',
  })
}
//获取用户拥有的菜单
export function getMenuByUserId(data) {
  return request({
    url: '/permission/getMenuByUserId',
    method: 'get',
    params:data
  })
}
//获取单条菜单
export function getMenuById(data) {
  return request({
    url: '/permission/getMenuById',
    method: 'get',
    params:data
  })
}
//新增菜单
export function addMenu(data) {
  return request({
    url: '/permission/addMenu',
    method: 'post',
    data:data
  })
}
//修改菜单
export function updateMenu(data) {
  return request({
    url: '/permission/updateMenu',
    method: 'post',
    data:data
  })
}
//删除菜单
export function deleteMenuById(data) {
  return request({
    url: '/permission/deleteMenuById',
    method: 'get',
    params:data
  })
}
//删除多条菜单
export function deleteMenuByIdList(data) {
  return request({
    url: '/permission/deleteMenuByIdList',
    method: 'get',
    params:data
  })
}
//获取菜单的父菜单的idList
export function getFaththIdListByid(data){
  return request({
    url: '/permission/getFaththIdListByid',
    method: 'get',
    params:data
  })
}