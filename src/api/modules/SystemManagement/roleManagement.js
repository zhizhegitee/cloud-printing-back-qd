import request from '@/utils/axiosRequest.js'

//获取所有角色
export function getAllRole() {
  return request({
    url: '/role/getAllRole',
    method: 'get',
  })
}

//根据角色id获取菜单
export function getRoleByRoleId(data) {
  return request({
    url: '/role/getRoleByRoleId',
    method: 'get',
    params:data
  })
}
//根据角色id获取菜单
export function updateRoleByRoleId(data) {
  return request({
    url: '/role/updateRoleByRoleId',
    method: 'post',
    data:data
  })
}
//修改角色
export function updateRole(data) {
  return request({
    url: '/role/updateRole',
    method: 'post',
    data:data
  })
}
//增加角色
export function addRole(data) {
  return request({
    url: '/role/addRole',
    method: 'post',
    data:data
  })
}
//删除角色
export function deleteRoleById(data) {
  return request({
    url: '/role/deleteRoleById',
    method: 'post',
    data:data
  })
}
//删除多个角色
export function deleteRoleByIdList(data) {
  return request({
    url: '/role/deleteRoleByIdList',
    method: 'post',
    data:data
  })
}