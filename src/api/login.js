import request from '@/utils/axiosRequest.js'

export function login(username, password) {
  return request({
    url: 'user/login',
    method: 'post',
    data: {
      username,
      password
    }
  })
}

export function logout() {
  return request({
    url: 'user/logout',
    method: 'get',
  })
}

export function getDataDisplay() {
  return request({
    url: 'dataDisplay/getDataDisplay',
    method: 'get',
  })
}
