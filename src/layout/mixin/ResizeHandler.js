import { mapState,mapMutations } from "vuex"

const WIDTH = 992;

export default {
  beforeMount() {
    window.addEventListener('resize', this.$_resizeHandler)
  },
  beforeDestroy() {
    window.removeEventListener('resize', this.$_resizeHandler)
  },
  computed: {
    ...mapState({
        isCollapse: state => state.system.isCollapse
    })
},
  methods: {
    ...mapMutations({
      updateCollapse: 'system/updateCollapse'
    }),
    $_resizeHandler() {
      // console.log(document.body.clientWidth)
      if(document.body.clientWidth < WIDTH){
        if(this.isCollapse === false){
          this.updateCollapse();
        }
      }else{
        if(this.isCollapse === true){
          this.updateCollapse();
        }
      }
    }
  }
}
