import {getMenuByUserId} from "@/api/modules/SystemManagement/menuManagement"
import store from '@/store';
const system = {
    namespaced: true,
    state: {
        // 初始加载状态
        init: false,
        // 菜单数据
        menuList: [],
    },
    getters: {
    },
    mutations: {
        SET_SYSTEM_MENU: (state,data) =>{
            state.init = true
            state.menuList = data
        },
        INIT_INIT: (state) => {
            state.init = false
        }
    },
    actions: {
        GetMenu({commit}){
            return new Promise((resolve,reject) =>{
                let userId =  store.state.user.userInfo.userId
                if(userId){
                    getMenuByUserId({id:userId,permissionId:0}).then(res=>{
                        commit('SET_SYSTEM_MENU',res.data.data)
                        resolve()
                    }).catch(error=>{
                        reject(error)
                    })
                }
            })
        }
    },
}

export default system