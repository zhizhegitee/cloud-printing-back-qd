const system = {
    namespaced: true,
    state: {
        isCollapse:false,
        sidebarLogo: true,
    },
    getters: {},
    mutations: {
        updateCollapse(state) {
            state.isCollapse = !state.isCollapse
        },
        updateSidebarLogo(state) {
            state.sidebarLogo = !state.sidebarLogo
        }
    },
    actions: {},
}

export default system