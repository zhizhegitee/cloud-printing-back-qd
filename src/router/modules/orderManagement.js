import Layout from '@/layout/Index'
const orderManagementRoute = {
    path: '/orderManagement',
    component: Layout,
    redirect: '/orderManagement/order',
    name: 'orderManagement',
    meta:{title: '订单管理'},
    children: [
      {
        path: 'order',
        name: 'order',
        component: () => import('@/views/orderManagement/Order.vue'),
        meta:{title: '订单'},
      },
      {
        path: 'evaluationManagement',
        name: 'evaluationManagement',
        component: () => import('@/views/orderManagement/EvaluationManagement.vue'),
        meta:{title: '评价管理'},
      },
      {
        path: 'orderDetails/:orderId/',
        name: 'orderDetails',
        component: () => import('@/views/orderManagement/components/OrderDetails.vue'),
        meta:{title: '订单详情'},
      },
    ]
}
export default orderManagementRoute