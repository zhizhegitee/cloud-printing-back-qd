import Layout from '@/layout/Index'

const systemManagementRouter = {
  path: '/systemManagement',
  component: Layout,
  redirect: '/systemManagement/roleManagement',
  name: 'systemManagement',
  meta:{title: '系统管理'},
  children: [
    {
      path: 'roleManagement',
      name: 'roleManagement',
      component: () => import('@/views/SystemManagement/RoleManagement.vue'),
      meta:{title: '角色管理'},
    },
    {
      path: 'userManagement',
      name: 'userManagement',
      component: () => import('@/views/SystemManagement/UserManagement.vue'),
      meta:{title: '用户管理'},
    },
    {
      path: 'menuManagement',
      name: 'menuManagement',
      component: () => import('@/views/SystemManagement/MenuManagement.vue'),
      meta:{title: '菜单管理'},
    }
  ]
}
export default systemManagementRouter